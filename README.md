# Rusholution Time
A Python solver for the 
[Rush Hour](https://en.wikipedia.org/wiki/Rush_Hour_(board_game)) sliding block
puzzle. I constrained myself to spending 8 hours maximum for work on it.

## Install
It is as simple as cloning the repository. The CLI portion of the solver has no
library depencies other than what Python comes with out-of-the-box.
  
The web viewer is served by Flask. You can install Flask by running
`pip install flask`. The viewer can then be used via `flask run`.