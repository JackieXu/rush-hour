# Thoughts
A chronological list of thoughts and remarks on and during the Rush Hour puzzle
task.

## Getting started
So... Rush Hour. Pretty standard game, the 6x6 board should be easily solvable
using breadth-first search through possible state transitions from the starting
state. Shouldn't need any specific Python libraries besides `collections` for
its queue implementation. I could use the standard `list` datastructure, but
its performance on similar functions is bad compared to an actual queue.

## Pruning is your friend
I originally ran the BFS method without any checks for states that were already
visited, resulting in all the cars driving back and forth on a parking lot,
with drivers getting progressively more angry, and not really getting anywhere.
Storing a list of game state matrices could end really big (and slow to check),
so I 'hashed' the states by converting them to strings.

## Skipping classes
Classes have a heavier footprint than the standard dictionaries and lists, and
with the brevity of this puzzle, they don't add all that much readability.

## Viewer
I wrote a simple state renderer using canvas as a drawing platform.
Unfortunately no animations, but I wouldn't recommend animating within canvas
anyway, within the 2D context that is. Ideally I would have gone the SVG route,
however I wanted to keep it within the timeframe of eight hours. The viewer
reads JSON from a file, which contains the path taken in state form. To serve
these files, I used Flask to quickly get a minimalistic API running.


## Improvements for the future
So many! First of all, the current web viewer kind of hurts my soul. I built
it early on during the puzzle and intended to refactor later on. Unfortunately
time constraints blocked me on that one. At the moment it's a simple state
rendering machine drawing on a canvas. I was thinking of doing CSS animated
cars in a flexbox (by removing and adding elements, leaving the system to
reposition itself). Another interesting way would be SVG, also enabling an
interesting suite of animation techniques and more refined control.
  
Having the web viewer not simply as a viewer, but also function as the puzzle
itself for the user could have been fun. That way the solutions could be used
as hints. However, the web server would have to be rewritten to accept external
states and these then need to be sent to the solver.

Sound could have been fun! Adding in sliding sounds, and perhaps some tunes
on completion.

The graphics are... abysmal. Actually having car textures would have given
the viewer a whole different vibe.

On the solver side, a plethora of ways to speed it up. At the moment it takes
my Macbook 5 seconds to solve the 93 step puzzle; that's way too long!

The simplest would be converting standard Python list and their operations to
their equivalents within Numpy, to skip object access within arrays and benefit
from the principle of locality.  
  
Another would be to add heuristics for something like A* search. The current
BFS implementation is really not all that smart. We could prioritize states
that move cars blocking the red car and/or its distance to the exit.

Yet another interesting implementation would be using a genetic algorithm, or
something like ant colony optimization.