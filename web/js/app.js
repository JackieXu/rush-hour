(function (window, document) {
    'use strict'

    var colors = {
        A: '#006400',
        B: '#9932CC',
        C: '#556B2F',
        D: '#483D8B',
        E: '#00CED1',
        F: '#FF1493',
        G: '#B22222',
        H: '#DAA520',
        I: '#ADFF2F',
        J: '#4B0082',
        K: '#F08080',
        L: '#20B2AA',
        M: '#800000',
        N: '#BA55D3',
        O: '#C71585',
        P: '#FF4500',
        Q: '#CD853F',
        r: '#FF0000',
        '.': '#000000'
    }

    function Game(canvas, gameId) {
        var self = this

        this.canvas = canvas
        this.context = canvas.getContext('2d')

        $.getJSON('http://localhost:5000/game/' + gameId, function (data) {
            self.solvable = data['solvable']
            self.history = data['history']
            self.state_index = 0

            if (!self.solvable) {
                alert('This puzzle has no solution.')
            }

            self.render()
        })
    }

    Game.prototype.render = function () {
        var state = this.history[this.state_index]

        for (var row = 0; row < state.length; row++) {
            for (var column = 0; column < state.length; column++) {
                this.context.fillStyle = colors[state[row][column]]
                this.context.fillRect(column * 100, row * 100, 100, 100)
            }
        }
    }

    var game = new Game($('#game-viewer')[0], 1),
        backButton = $('#game-viewer-controls-back-button'),
        forwardButton = $('#game-viewer-controls-forward-button')

    backButton.on('click', function () {
        if (game.state_index > 0) {
            game.state_index--
            game.render()
        }
    })

    forwardButton.on('click', function () {
        if (game.state_index < game.history.length - 1) {
            game.state_index++
            game.render()
        }
    })

}(window, document))