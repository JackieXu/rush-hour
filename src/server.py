from flask import Flask, Response
from json import dumps, load

app = Flask(__name__)


@app.route('/game/<int:game_id>')
def game(game_id):
    with open('../data/states_%d.txt' % game_id, 'r') as json_file:
        json_data = load(json_file)

        response = Response(dumps(json_data))
        response.headers['Access-Control-Allow-Origin'] = '*'

        return response

if __name__ == '__main__':
    app.run()
