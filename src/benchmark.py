from time import time


def timed(method):
    """
    Times and prints function call speeds.

    :param method:  Method to time.
    :return:
    """
    def wrapper(*args, **kwargs):
        start = time()
        result = method(*args, **kwargs)
        end = time()

        print end - start, 's'

        return result

    return wrapper
