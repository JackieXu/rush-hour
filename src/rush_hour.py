from benchmark import timed
from collections import deque


def get_board(file_object):
    """
    Reads board from a stream.

    Example board:
    [
        ['.', '.', '.', '.', '.', '.'],
        ['.', '.', 'A', '.', '.', '.'],
        ['r', 'r', 'A', '.', '.', '.'],
        ['.', '.', '.', 'B', '.', '.'],
        ['.', '.', '.', 'B', '.', '.'],
        ['.', '.', '.', '.', '.', '.'],
    ]

    :param file_object: The input stream to read from.
    :return:            A two-dimensional list of spaces taken or not taken in by cars.
    """
    dimensions = next(file_object)
    height = int(dimensions.strip().split(' ')[1])

    return [list(next(file_object).strip()) for _ in xrange(height)]


def get_vehicles(board):
    """
    Reads vehicle data from a board and returns a list of vehicles.

    Vehicles are either orientated horizontally or vertically, indicated respecively
    by 'H' and 'V'.

    Example vehicle dictionary object:
    {
        'id': 'A',
        'row': 2,
        'column': 4,
        'length': 3,
        'orientation': 'V'
    }

    See `get_board(file_object)` for board representation.

    TODO: Check if it's faster to stop reading vehicle data from boards. Recreate board
    from vehicle list data.

    :param board:   The board to read from.
    :return:        A list of vehicle dictionaries.
    """
    vehicles = {}

    for row, line in enumerate(board):
        for column, vehicle_id in enumerate(line):
            if vehicle_id is '.':
                continue

            if vehicle_id in vehicles:
                vehicle = vehicles[vehicle_id]
                vehicle['length'] += 1

                if vehicle['orientation'] is None:
                    vehicle['orientation'] = 'V' if row > vehicle['row'] else 'H'

            else:
                vehicles[vehicle_id] = {
                    'id': vehicle_id,
                    'row': row,
                    'column': column,
                    'length': 1,
                    'orientation': None
                }

    return vehicles.values()


def get_possible_states(board, vehicles):
    """
    Gets all new possible states.

    This method checks which vehicles can move, and if so yields the state
    that would result out of that move.

    TODO: Fix the wall of text in the if-conditions; it's ugly.

    :param board:       The current board
    :param vehicles:    List of vehicles on the board
    :return:
    """
    for vehicle in vehicles:
        if vehicle['orientation'] == 'V':
            if vehicle['row'] > 0 and board[vehicle['row'] - 1][vehicle['column']] == '.':
                board_copy = [row[:] for row in board[:]]
                board_copy[vehicle['row'] + vehicle['length'] - 1][vehicle['column']] = '.'
                board_copy[vehicle['row'] - 1][vehicle['column']] = vehicle['id']
                yield board_copy
            if vehicle['row'] + vehicle['length'] < len(board) \
                    and board[vehicle['row'] + vehicle['length']][vehicle['column']] == '.':
                board_copy = [row[:] for row in board[:]]
                board_copy[vehicle['row']][vehicle['column']] = '.'
                board_copy[vehicle['row'] + vehicle['length']][vehicle['column']] = vehicle['id']
                yield board_copy
        else:
            if vehicle['column'] > 0 and board[vehicle['row']][vehicle['column'] - 1] == '.':
                board_copy = [row[:] for row in board[:]]
                board_copy[vehicle['row']][vehicle['column'] + vehicle['length'] - 1] = '.'
                board_copy[vehicle['row']][vehicle['column'] - 1] = vehicle['id']
                yield board_copy
            if vehicle['column'] + vehicle['length'] < len(board[0]) \
                    and board[vehicle['row']][vehicle['column'] + vehicle['length']] == '.':
                board_copy = [row[:] for row in board[:]]
                board_copy[vehicle['row']][vehicle['column']] = '.'
                board_copy[vehicle['row']][vehicle['column'] + vehicle['length']] = vehicle['id']
                yield board_copy


@timed
def solve(file_object):
    """
    Attempts to solve the Rush Hour puzzle.

    :param file_object: The input stream.
    :return:            A list of states, from start to solution.
    """
    board = get_board(file_object)

    queue = deque()
    queue.appendleft([board])

    seen_states = set()

    while queue:
        states = queue.pop()
        latest_board = states[-1]

        if str(latest_board) in seen_states:
            continue

        if latest_board[2][4] == 'r' and latest_board[2][5] == 'r':
            return states

        seen_states.add(str(latest_board))

        for possible_state in get_possible_states(latest_board, get_vehicles(latest_board)):
            if possible_state not in states:
                branch = states + [possible_state]
                queue.appendleft(branch)

    return None


def get_moves_from_states(states):
    """
    Gets human-readable moves from list of states.

    :param states:  List of states, from start to solution.
    :return:        A human-readable format of steps to take to solve the puzzle.
    """
    if len(states) < 2:
        yield 'Free parking'

    previous_state = states[0]

    for state in states[1:]:
        previous_vehicle_state = get_vehicles(previous_state)
        current_vehicle_state = get_vehicles(state)

        vehicle = [vehicle for vehicle in current_vehicle_state if vehicle not in previous_vehicle_state][0]
        previous_vehicle = [p_vehicle for p_vehicle in previous_vehicle_state if p_vehicle['id'] == vehicle['id']][0]

        if vehicle['orientation'] == 'V':
            yield '%s-%s' % (vehicle['id'], 'down' if vehicle['row'] > previous_vehicle['row'] else 'up')
        else:
            yield '%s-%s' % (vehicle['id'], 'right' if vehicle['column'] > previous_vehicle['column'] else 'left')

        previous_state = state


if __name__ == '__main__':
    from json import dumps

    with open('../data/test_set.txt', 'r') as test_boards_file, \
            open('../data/test_set_out.txt', 'w') as test_solutions_file:
        cases = int(next(test_boards_file))

        for case_number in range(cases):
            with open('../data/states_%d.txt' % case_number, 'w') as json_file:
                solution = solve(test_boards_file)
                test_solutions_file.write('#%d ' % (case_number + 1))

                if solution:
                    json_file.write(dumps({
                        'solvable': True,
                        'history': solution
                    }))
                    test_solutions_file.write('%d\n' % (len(solution) - 1))
                    test_solutions_file.write('\n'.join(get_moves_from_states(solution)) + '\n')
                else:
                    json_file.write(dumps({
                        'solvable': False,
                        'history': []
                    }))
                    test_solutions_file.write('No solution\n')

                json_file.flush()
                json_file.close()

        test_solutions_file.flush()
        test_solutions_file.close()
